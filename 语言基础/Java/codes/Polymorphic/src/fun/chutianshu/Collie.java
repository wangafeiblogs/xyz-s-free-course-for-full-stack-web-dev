package fun.chutianshu;

public class Collie extends Dog{
    public Collie() {
    }

    public Collie(String name, String type) {
        super(name, type);
    }
    //覆盖基类中同名方法
    @Override
    public void Play(){
        System.out.println("一只名叫"+this.getName()+"的"+this.getType()+"正在陪你玩飞盘~~");
    }
}
