package fun.chutianshu;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //为了体现多态的核心用法,将子类对象存入父类容器，通过父类的对象名来执行子类方法
        //定义一个父类数组
        Dog [] dogsArray = new Dog[3];
        dogsArray[0] = new Husky("二哈","哈士奇");
        dogsArray[1] = new Collie("奶牛","边牧");
        dogsArray[2] = new Collie("太极","边牧");

        //通过循环，调用多态代码
        for(int i = 0 ;i<dogsArray.length;i++)
        {
            dogsArray[i].Play();
        }

        // 抽象类不能够用来创建实体对象,下面两行代码是错误的：
        //Dog d1 = new Dog();
        //Dog d2 = new Dog("小白","京巴");

        //创建FireMonster 接口数组
        FireSkill[] fireMonstersArray = new FireSkill[2];

        fireMonstersArray[0] = new Husky("三哈","哈士奇");
        //如果一个类中，没有定义任何构造方法，系统会自动为我们添加一个空构造方法
        fireMonstersArray[1] = new Dragon();

        //通过循环，调用多态代码
        for(int i = 0 ;i<fireMonstersArray.length;i++)
        {
            fireMonstersArray[i].Fire();
        }

        //值类型
        int a =5;
        int b =a;
        System.out.println("a="+a+",b="+b);
        b =10;
        System.out.println("a="+a+",b="+b);

        Dog dog1 = new Husky();
        Dog dog2 = dog1;
        System.out.println("dog1.name="+dog1.getName()+";dog2.name="+dog2.getName());
        dog2.setName("二哈");
        System.out.println("dog1.name="+dog1.getName()+";dog2.name="+dog2.getName());


    }
}
