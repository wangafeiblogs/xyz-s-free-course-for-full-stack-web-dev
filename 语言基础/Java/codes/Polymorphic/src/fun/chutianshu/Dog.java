package fun.chutianshu;

public abstract class Dog {
    private String name;
    private String type;

    public Dog(){}
    public Dog(String name, String type) {
        this.name = name;
        this.type = type;
    }

    //通用的基类中方法
    // 1. 在类中，使用不到的方法，都可以作为抽象方法
    // 2. 如果一个类中，存在了抽象方法，这个类就必须也设置为抽象类
    // 3. 抽象类不能够定义实体对象，像这种代码是错误的： Dog d1 = new Dog()
    public abstract void Play();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
