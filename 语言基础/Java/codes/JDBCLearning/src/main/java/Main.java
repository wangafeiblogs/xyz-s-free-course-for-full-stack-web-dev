import java.sql.*;



public class Main {
    public static void main(String[] args) throws SQLException {
        //初始化，并创建出操作数据的帮助类，会直接操作默认本机数据库
        SqlHelper sqlHelper = new SqlHelper();

        // 插入新的数据信息
        sqlHelper.addStudent("花花",0,2,99);

        //查询并打印二年级女生信息
        sqlHelper.printAllStudents(0,2);

        //关闭数据连接，释放资源
        sqlHelper.closeDb();

    }
}
