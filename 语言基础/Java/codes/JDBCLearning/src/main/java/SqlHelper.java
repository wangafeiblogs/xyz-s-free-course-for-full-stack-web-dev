import java.sql.*;

/***
 *
 * 新建一个专门用来处理数据操作的类
 *
 *
 */
public class SqlHelper {
    //直接在类的字段定义时，就为其赋值，这个值就会称为“默认值”：如果在后面的操作中，比如构造方法中没有为字段赋值，则直接使用默认值
    // 连数据库的几个参数
    //数据库连接地址
    private String JDBC_URL = "jdbc:mysql://localhost:3306/learnjdbc?useSSL=false&characterEncoding=utf8";
    //数据库用户名
    private String JDBC_USER = "root";
    //数据库密码
    private String JDBC_PASSWORD = "root";

    // 数据库连接对象
    private Connection conn;

    //全参数的构造方法，用来连接非默认数据库
    public SqlHelper(String JDBC_URL, String JDBC_USER, String JDBC_PASSWORD) throws SQLException {
        this.JDBC_URL = JDBC_URL;
        this.JDBC_USER = JDBC_USER;
        this.JDBC_PASSWORD = JDBC_PASSWORD;
        conn = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
    }
    //无参数的构造方法，用来连接默认本机的数据库
    public SqlHelper() throws SQLException {
        conn = DriverManager.getConnection(this.JDBC_URL, this.JDBC_USER, this.JDBC_PASSWORD);
    }

    public String getJDBC_URL() {
        return JDBC_URL;
    }

    public void setJDBC_URL(String JDBC_URL) {
        this.JDBC_URL = JDBC_URL;
    }

    public String getJDBC_USER() {
        return JDBC_USER;
    }

    public void setJDBC_USER(String JDBC_USER) {
        this.JDBC_USER = JDBC_USER;
    }

    public String getJDBC_PASSWORD() {
        return JDBC_PASSWORD;
    }

    public void setJDBC_PASSWORD(String JDBC_PASSWORD) {
        this.JDBC_PASSWORD = JDBC_PASSWORD;
    }

    //数据库的操作，增删该差等，都可以写到下面

    //查询操作，获取指定性别,指定年级的 学生信息
    public void printAllStudents(int gender,int grade) throws SQLException {
        //数据库状态管理对象，用来操作数据查询
        //PreparedStatement 相对于 普通的 Statement ，可以防止 SQL 注入
        // 他可以进行数据库查询操作时的参数检查和处理
        PreparedStatement ps = conn.prepareStatement("SELECT id, grade, name, gender FROM students WHERE gender=? and grade = ?");
        //根据索引准备参数，索引从 1 开始
        ps.setObject(1, gender);
        //准备年级参数
        ps.setObject(2,grade);

        //执行后的查询结果，存入结果集
        ResultSet rs = ps.executeQuery();
        //结果集可以通过指针，一行一行获取数据
        while (rs.next()) {
            //结果集对象可以将指针指向的当前行的每一列的数据读取出来
            long id = rs.getLong("id"); // 注意：索引从1开始
            String name = rs.getString("name");
            System.out.println("当前行数据为：id="+id+",grade="+grade+",name="+name+",gender="+gender);
        }
    }

    // 关闭数据库连接
    public void closeDb() throws SQLException {
        this.conn.close();
    }

    //插入新的学生信息到数据库
    public int addStudent(String name,int gender,int grade,int score) throws SQLException {
        //创建数据库状态管理对象
        PreparedStatement ps = conn.prepareStatement(
                "INSERT INTO students (score, grade, name, gender) VALUES (?,?,?,?)");
        //准备参数
        ps.setObject(1,score);
        ps.setObject(2,grade);
        ps.setObject(3,name);
        ps.setObject(4,gender);
        //执行数据操作，返回的整型是收到影响的数据库行数
        int n = ps.executeUpdate();
        return n;
    }

}
