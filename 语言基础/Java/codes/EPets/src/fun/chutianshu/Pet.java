package fun.chutianshu;

/**
 * 所有宠物的基类
 * 其中包含宠物都有的字段和方法
 */
public class Pet {
    //寂寞值
    public int loneliness=50;

    //陪宠物玩
    public void Play(){
        loneliness--;
        System.out.println("陪宠物玩耍，宠物寂寞值减少，当前寂寞值："+this.loneliness);
    }

}
