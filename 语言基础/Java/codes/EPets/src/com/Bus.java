package com;

public class Bus extends Vehicle {
    private int seat;
    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }
    public Bus(String type, String brand, String no, double money, int seat) {
        super(type, brand, no, money);
        this.seat = seat;
    }
    @Override
    public double calRent(int days) {
        //折扣
        double discount=1;
        if(days<3){
            discount=1;
        }else if(days>=3&&days<7){
            discount=0.9;
        }else if(days>=7&&days<30){
            discount=0.8;
        }else if(days>=30&&days<150){
            discount=0.7;
        }else{
            discount=0.6;
        }
        return this.getMoney()*days*discount;
    }
}