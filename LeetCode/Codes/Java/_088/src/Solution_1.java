public class Solution_1 {
    //正向算法，思路比较麻烦，但时间复杂度很低
    //空间复杂度稍高，多了一个新数组
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if(m==0)
        {
            nums1=nums2;
            return;
        }
        if(n==0)
        {
            return;
        }
        int [] nums3=new int[m+n];
        int i=0,j=0,k=0;
        while(k<m+n)
        {
            if(i<m) {
                if(j<n) {
                    if (nums1[i] < nums2[j]) {
                        nums3[k] = nums1[i];
                        i++;
                        k++;
                    } else {
                        nums3[k] = nums2[j];
                        j++;
                        k++;
                    }
                }
                else {
                    nums3[k]=nums1[i];
                    i++;
                    k++;
                }
            }
            else {
                nums3[k] = nums2[j];
                j++;
                k++;
            }
        }
        nums1=nums3;
        for (int x:nums3) {
            System.out.println(x);
        }
        for (int x:nums1) {
            System.out.println(x);
        }
    }
}
