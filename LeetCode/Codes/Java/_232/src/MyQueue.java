import java.util.Stack;

/*
* 思路：
* 在两个栈之间倒腾就行了
*
*
*
*
* */

public class MyQueue {
    // 栈data1，存储数据时，栈顶为队列尾部，栈底为队列头部
    private Stack<Integer> data1=new Stack<>();
    // 栈data2，是data1出栈时的产物，所以方向跟data1正好相反，栈顶为队列头部，栈底为队列尾部
    private Stack<Integer> data2=new Stack<>();
    public MyQueue() {
    }

    //push时，先应确保所有数据都在data1中
    public void push(int x) {
        while (!data2.empty())
        {
            data1.push(data2.pop());
        }
        data1.push(x);
    }
    //pop和peek时，应该把数据先导入data2
    public int pop() {
        while(!data1.empty())
        {
            data2.push(data1.pop());
        }
        return data2.pop();
    }

    public int peek() {
        while(!data1.empty())
        {
            data2.push(data1.pop());
        }
        return data2.peek();
    }

    public boolean empty() {
        if(data1.empty()&&data2.empty())
        {
            return true;
        }
        else {
            return false;
        }
    }
}
