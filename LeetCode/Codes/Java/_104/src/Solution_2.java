/*
* 算法：递归
*
* 思路：借助 Queue
* 如果我们知道了左子树和右子树的最大深度 l 和 r，那么该二叉树的最大深度即为
* max(l,r) + 1
* 而左子树和右子树的最大深度又可以以同样的方式进行计算。
*
* */


import java.util.LinkedList;
import java.util.Queue;

public class Solution_2 {
    public int maxDepth(TreeNode root) {
        if(root==null)
        {
            return 0;
        }

        int l=0;
        int r=0;
        if(root.right!=null)
        {
            r=maxDepth(root.right);
        }
        if(root.left!=null)
        {
            l=maxDepth(root.left);
        }



        return l>r?l+1:r+1;
    }
}
