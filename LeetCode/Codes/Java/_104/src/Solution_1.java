/*
* 算法：遍历迭代
*
* 思路：借助 Queue
*
*
* */


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Solution_1 {

    public int maxDepth(TreeNode root) {
        if(root==null)
        {
            return 0;
        }

        Queue<TreeNode> thisLevelQueue =new LinkedList<>();
        Queue<TreeNode> subLevelQueue = new LinkedList<>();

        thisLevelQueue.offer(root.left);
        thisLevelQueue.offer(root.right);

        TreeNode point = new TreeNode();
        int depth = 0;
        while (!thisLevelQueue.isEmpty())
        {
            point = thisLevelQueue.poll();
            if(point!=null)
            {
                subLevelQueue.offer(point.left);
                subLevelQueue.offer(point.right);
            }
            if(thisLevelQueue.isEmpty())
            {
                thisLevelQueue=subLevelQueue;
                subLevelQueue=new LinkedList<>();
                depth++;
            }
        }


        return depth;
    }
}
