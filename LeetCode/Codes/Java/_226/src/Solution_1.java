/*
    算法：递归

    思路：把每个节点的左右子节点互换



 */

public class Solution_1 {
    public TreeNode invertTree(TreeNode root) {

        if(root==null)
        {
            return root;
        }

        TreeNode node = new TreeNode();

        //node = root;

        node = root.left;

        root.left = root.right;

        invertTree(root.left);

        root.right = node;

        invertTree(root.right);

        return root;
    }
}
