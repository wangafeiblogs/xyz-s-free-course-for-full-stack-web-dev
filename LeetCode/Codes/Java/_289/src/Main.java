public class Main {
    public static void main(String[] args) {
        int [][] board = {
                {0,1,0},
                {0,0,1},
                {1,1,1},
                {0,0,0}
        };
        System.out.println(board);
        Solution_1 solution = new Solution_1();
        solution.gameOfLife(board);
        System.out.println(board);
    }
}