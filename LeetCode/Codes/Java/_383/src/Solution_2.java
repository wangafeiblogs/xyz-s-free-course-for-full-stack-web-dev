//思路：节省一个数组，只在一个数组中比较
public class Solution_2 {
    public boolean canConstruct(String ransomNote, String magazine) {
        int [] m = new int[26];
        for(int i =0;i<magazine.length();i++)
        {
            int x = magazine.charAt(i);
            m[x-'a']++;
        }
        for(int i =0;i<ransomNote.length();i++)
        {
            int x = ransomNote.charAt(i);
            m[x-'a']--;
        }

        for(int i=0;i<26;i++)
        {
            if(m[i]<0)
            {
                return false;
            }
        }
        return true;
    }
}
