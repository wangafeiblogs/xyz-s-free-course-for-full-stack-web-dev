/*
*
*  给定一个 n × n 的二维矩阵 matrix 表示一个图像。请你将图像顺时针旋转 90 度。

你必须在 原地 旋转图像，这意味着你需要直接修改输入的二维矩阵。请不要 使用另一个矩阵来旋转图像。
*
*  分析：
*  按层次，每一层操作方式一样
*
*  单层操作方式：
*   1. 第一行是原第一列倒置；
*   2. 第二列是原第一行順置；
*   3. 第二行是原第二列倒置；
*   4. 第一列是原第二行順置
* */

public class Solution_01 {

    public void rotate(int[][] matrix) {
        int n=matrix.length;

        //直接返回
        if(n<=1)
        {
            return;
        }

        //算出层数
        int x = n/2;

        //每次循环操作一层，由外到内
        for(int i = 0;i<x;i++)
        {
            //内层循环进行当前层次数值轮换
            for(int j=0;j<n-i-1;j++)
            {

                int tmp = matrix[i][j];
                matrix[i][j]=matrix[n-j-1][i];
                matrix[n-j-1][i]=matrix[n-i-1][n-j-1];
                matrix[n-i-1][n-j-1]=matrix[j][n-i-1];
                matrix[j][n-i-1]=tmp;
            }
        }
    }

}
