import java.util.Stack;

/*
    思路：
    因为题目中的括号牵扯到嵌套

*/
public class Solution_1 {
    public boolean isValid(String s) {
        if(s==null||s.length()<2)
        {
            return false;
        }
        char [] chars = s.toCharArray();
        Stack<Character> characterStack = new Stack<>();
        for (char c: chars) {
            if(characterStack.empty())
            {
                characterStack.push(c);
            }
            else {
                char stackTopChar = characterStack.peek();
                if((c == stackTopChar+1||c==stackTopChar+2))
                {
                    characterStack.pop();
                }
                else{
                    characterStack.push(c);
                }
            }
        }
        if(characterStack.empty())
        {
            return true;
        }
        else {
            return false;
        }

    }
}
