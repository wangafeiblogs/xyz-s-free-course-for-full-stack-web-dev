import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Solution_3 {
    public boolean containsDuplicate(int[] nums){

        Set<Integer> set = new HashSet<Integer>();
        for (int n: nums) {
            if(!set.add(n)) {
                return true;
            }
        }
        return false;
    }
}
