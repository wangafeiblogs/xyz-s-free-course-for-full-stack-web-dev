public class TreeNode{
    public int val;
    public TreeNode left;
    public TreeNode right;
    TreeNode(){}

    TreeNode(int val){
        this.val=val;
    }
    TreeNode(int val,TreeNode left,TreeNode right){
        val = val;
        left=left;
        right=right;
    }
}
