
// 思路：
// 1. 判断是否是 1-9 字符（可选）
// 2. 判断每行、列，不是 '.' 入 HashSet，若已存在，报错
// 3. 判断每个 3*3
// 优点：思路简单；缺点：遍历次数较多，需遍历数组3次
import java.util.HashSet;

public class Solution_1 {
    public boolean isValidSudoku(char[][] board) {

        // 判断行
        for(int i = 0 ;i<9;i++)
        {
            HashSet<Character> hashSet = new HashSet<>();
            for(int j = 0;j<9;j++)
            {

                char c = board[i][j];
                if(c!='.')
                {
                    if(!hashSet.add(c)){
                        return false;
                    }
                }
            }
        }
        // 判断列
        for(int i = 0 ;i<9;i++)
        {
            HashSet<Character> hashSet = new HashSet<>();
            for(int j = 0;j<9;j++)
            {

                char c = board[j][i];
                if(c!='.')
                {
                    if(!hashSet.add(c)){
                        return false;
                    }
                }
            }
        }
        //判断3*3子矩阵
        for(int h=0;h<3;h++) {
            for (int i = 0; i < 3; i++) {
                HashSet<Character> hashSet = new HashSet<>();
                for (int j = 0; j < 3; j++) {
                    for (int k = 0; k < 3; k++) {
                        char c = board[3*h+j][3*i+k];
                        if (c != '.') {
                            if (!hashSet.add(c)) {
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return true;
    }
}
