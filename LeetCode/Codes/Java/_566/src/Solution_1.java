public class Solution_1 {
    public int[][] matrixReshape(int[][] mat, int r, int c) {
        //行数
        int m = mat.length;
        //列数
        int n = mat[0].length;
        int [][] array = new int[r][c];
        if(m*n==r*c)
        {
            for(int i = 0;i<m;i++)
            {
                for(int j=0;j<n;j++)
                {
                    int num = (n*i+j);
                    //行
                    int x = num/c;
                    //列
                    int y = num%c;
                    //赋值
                    array[x][y]=mat[i][j];
                }
            }
            return array;
        }
        return mat;
    }
}
