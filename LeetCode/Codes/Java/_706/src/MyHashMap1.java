/*
不使用任何内建的哈希表库设计一个哈希映射（HashMap）。

实现 MyHashMap 类：

MyHashMap() 用空映射初始化对象
void put(int key, int value) 向 HashMap 插入一个键值对 (key, value) 。如果 key 已经存在于映射中，则更新其对应的值 value 。
int get(int key) 返回特定的 key 所映射的 value ；如果映射中不包含 key 的映射，返回 -1 。
void remove(key) 如果映射中存在 key 的映射，则移除 key 和它所对应的 value 。

解释：
MyHashMap myHashMap = new MyHashMap();
myHashMap.put(1, 1); // myHashMap 现在为 [[1,1]]
myHashMap.put(2, 2); // myHashMap 现在为 [[1,1], [2,2]]
myHashMap.get(1);    // 返回 1 ，myHashMap 现在为 [[1,1], [2,2]]
myHashMap.get(3);    // 返回 -1（未找到），myHashMap 现在为 [[1,1], [2,2]]
myHashMap.put(2, 1); // myHashMap 现在为 [[1,1], [2,1]]（更新已有的值）
myHashMap.get(2);    // 返回 1 ，myHashMap 现在为 [[1,1], [2,1]]
myHashMap.remove(2); // 删除键为 2 的数据，myHashMap 现在为 [[1,1]]
myHashMap.get(2);    // 返回 -1（未找到），myHashMap 现在为 [[1,1]]

分析：
在这个题目的要求中，MyHashMap 的 Entry 使用 int [2] 来保存，整个 MyHashMap 使用 int[] 来存储，所以 MyHashMap 的数据结构是 int [][2]

算法：
此算法不推荐，牵扯到大量数组和列表间转换，时间复杂度太高，空间也浪费太多。虽然正确，但时间上无法通过


 */

import java.util.ArrayList;
import java.util.List;

public class MyHashMap1 {
    //问题：如何处理动态数组大小
    int[][] data;
    public MyHashMap1() {
        List<List<Integer>> arrayList = new ArrayList<>();
        data = arrayListToArray(arrayList);
    }

    public void put(int key, int value) {
        List<List<Integer>> arrayList = arrayToList(data);
        for (List<Integer> x:arrayList) {
            if(x.get(0)==key)
            {
                x.set(1,value);
                this.data = arrayListToArray(arrayList);
                return;
            }
        }
        List<Integer> a = new ArrayList<>();
        a.add(key);
        a.add(value);
        arrayList.add(a);
        this.data = arrayListToArray(arrayList);
    }

    public int get(int key) {
        for (int i=0;i<data.length;i++) {
            if(data[i][0]==key)
            {
                return data[i][1];
            }
        }
        return -1;
    }

    public void remove(int key) {
        List<List<Integer>> arrayList = arrayToList(data);
        for (List<Integer> x:arrayList) {
            if(x.get(0)==key)
            {
                arrayList.remove(x);
                this.data = arrayListToArray(arrayList);
                return;
            }
        }
    }


    private int[][] arrayListToArray(List<List<Integer>> arrayList){
        int [][] result = new int[arrayList.size()][];
        for (int i =0;i<arrayList.size();i++) {
            int[] a = {arrayList.get(i).get(0),arrayList.get(i).get(1)};
            result[i]=a;
        }
        return result;
    }

    private List<List<Integer>> arrayToList(int[][] array)
    {
        List<List<Integer>> arrayList = new ArrayList<>();
        for(int i = 0 ;i<array.length;i++)
        {
            List<Integer> al = new ArrayList<>();
            al.add(array[i][0]);
            al.add(array[i][1]);
            arrayList.add(al);
        }
        return arrayList;
    }

}
