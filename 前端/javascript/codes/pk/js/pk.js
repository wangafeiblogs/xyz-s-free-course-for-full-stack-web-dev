//玩家对象
var player = {
	name : "李逍遥",
	hp:100,
	ap:10
}

//怪物对象
var monster = {
	name : "蛤蟆精",
	hp:50,
	ap:5
}

//互殴函数
function pk(a,b){
	while(true)
	{
		//a 先打 b
		attack(a,b)
		if(b.hp<=0) return
		attack(b,a)
		if(a.hp<=0) return
	}
}

// 一次攻击
function attack(a,b)
{
	//a 先打 b
	b.hp-=a.ap;
	console.log(a.name+"攻击"+b.name+",打掉"+a.ap+"点血，"+b.name+"剩余生命值："+b.hp)
	if(b.hp<=0)
	{
		console.log(b.name+"挂了。。。"+a.name+"胜利！")
	}
}

//开始游戏
function play(){
	pk(player,monster)
}